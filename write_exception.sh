# set enviroment variable
#p=ssh
echo "type in target name"

# read target variable
read p

## get avc list
grep $p /var/log/audit/audit.log | audit2allow -l

## turn asking for continue
echo "Okay -  do you want to continue and write the rule to system?(yes/no)"
read input
if [ "$input" == "yes" ]
then

## create module
grep $p /var/log/audit/audit.log | audit2allow -l -v -m local_$p > local_$p.te

## check module (will create .mod)
checkmodule -M -m -o local_$p.mod local_$p.te

## build module
semodule_package -o local_$p.pp -m local_$p.mod

## install module
semodule -v -i local_$p.pp

## ending turn
fi

## remove enviroment variable
unset p

## bye
echo "bye bye"
